package com.piotr.main;

import com.piotr.main.PlatformaMotywacyjnaApplication;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = PlatformaMotywacyjnaApplication.class)
@AutoConfigureMockMvc
@Import(PlatformaMotywacyjnaApplication.class)
public class PlatformaMotywacyjnaApplicationTests {


    //
    @Test
    public void contextLoad() throws Exception {
    }




}
