package com.piotr.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.piotr.DTO.LoginPacket;
import com.piotr.DTO.TransferMoneyPacket;
import com.piotr.DTO.UserPacket;
import com.piotr.entities.CourseCategory;
import com.piotr.entities.User;
import com.piotr.main.PlatformaMotywacyjnaApplicationTests;
import com.piotr.repository.*;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.AssertTrue;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class UserControllerTest extends PlatformaMotywacyjnaApplicationTests {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private CourseCategoryRepository courseCategoryRepository;

    @Autowired
    private LessonCourseRepository lessonCourseRepository;

    @Autowired
    private AnswerRepository answerRepository;

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private ElementLessonRepository elementLessonRepository;

    @Autowired
    private LessonUserRepository lessonUserRepository;

    //
    @Transactional
    @Test
    public void MockMvcTest() throws Exception {
        Assert.assertNotNull(this.mvc);
    }

    @Transactional
    @Test
    public void hello() throws Exception {
        this.mvc.perform(get("/api/user/hello")).andExpect(status().isOk())
                .andExpect(content().string("czesc :)"));

    }

    @Transactional
    @Test
    public void registry() throws Exception {
        UserPacket userPacket = new UserPacket(900, "xyz", "haslo", 34, "image.png", 12, 23, 42, 23, 3, 2);
        ObjectMapper objectMapper = new ObjectMapper();
        this.mvc.perform(post("/api/user/update")
                .content(objectMapper.writeValueAsString(userPacket))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"));
    }

    @Transactional
    @Test
    public void deleteUser() throws Exception {
        User user = new User(900, "xyz", "haslo", 122, "image.png", 1, 120, 23, 32, 1, 32, null);
        userRepository.save(user);
        user = userRepository.findByUsername("xyz");

        this.mvc.perform(delete("/api/user/delete/" + user.getId())).andExpect(status().isOk());


        user = userRepository.findByUsername("xyz");
        Assert.assertNull(user);
    }

    @Transactional
    @Test
    public void getUser() throws Exception {
        User user = new User(900, "xyz", "haslo", 122, "image.png", 1, 120, 23, 32, 1, 32, null);
        userRepository.save(user);
        user = userRepository.findByUsername("xyz");

        this.mvc.perform(get("/api/user/get/" + user.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"));

    }

    @Transactional
    @Test
    public void showAllUsers() throws Exception {
        this.mvc.perform(get("/api/user/showAllUsers"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"));

    }

    @Transactional
    @Test
    public void getAccountUser() throws Exception {
        User user = new User(900, "xyz", "haslo", 122, "image.png", 1, 120, 23, 32, 1, 32, null);
        userRepository.save(user);
        user = userRepository.findByUsername("xyz");

        LoginPacket loginPacket = new LoginPacket(user.getUsername(), user.getPassword());

        ObjectMapper objectMapper = new ObjectMapper();
        this.mvc.perform(post("/api/user/getAccountUser")
                .content(objectMapper.writeValueAsString(loginPacket))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"));
    }

    @Transactional
    @Test
    public void addMoney() throws Exception {
        User user = new User(900, "xyz", "haslo", 122, "image.png", 1, 120, 23, 32, 1, 32, null);
        userRepository.save(user);
        user = userRepository.findByUsername("xyz");

        TransferMoneyPacket transferMoneyPacket = new TransferMoneyPacket(user.getId(), 100);

        ObjectMapper objectMapper = new ObjectMapper();
        this.mvc.perform(post("/api/user/addMoney")
                .content(objectMapper.writeValueAsString(transferMoneyPacket))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("222.0"));


        user = userRepository.findByUsername("xyz");

        Assert.assertFalse(user.getMoney()!=222);


    }

////    @Test
////    public void hello() throws Exception {
////        this.mvc.perform(getUser("/api/user/hello")).andExpect(status().isOk())
////                .andExpect(content().string("czesc :)"));
////    }
//
//    @Test
//    public void correctAutowiredRepository () throws Exception {
//        Assert.assertNotNull(userRepository);
//        Assert.assertNotNull(courseRepository);
//        Assert.assertNotNull(courseCategoryRepository);
//        Assert.assertNotNull(lessonCourseRepository);
//        Assert.assertNotNull(answerRepository);
//        Assert.assertNotNull(questionRepository);
//        Assert.assertNotNull(elementLessonRepository);
//        Assert.assertNotNull(lessonUserRepository);
//    }
//
//
//
//
////    @Test
////    public void createNewUser() throws Exception {
////        long licznik_przed = userRepository.count();
//////        User user = new User((int)licznik_przed + 1,"Jan","haslo",0.2,null,null);
////
////        ObjectMapper objectMapper = new ObjectMapper();
////        this.mvc.perform(post("/user/create").content(objectMapper.writeValueAsString(user)).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
////                .andExpect(content().contentType("application/json;charset=UTF-8"))
////                .andExpect(jsonPath("$.id").value(user.getId())).andExpect(jsonPath("$.username").value(user.getUsername()))
////                .andExpect(jsonPath("$.password").value(user.getPassword())).andExpect(jsonPath("$.money").value(user.getMoney()));
////
////        long licznik_po = userRepository.count();
////        Assert.assertEquals(licznik_przed + 1, licznik_po);
////    }
//
//
//    @Test
//    public void showLastUser() throws Exception {
////        long licznik_przed = userRepository.count();
////        User user = userRepository.findById((int)licznik_przed).getUser();
////
////        this.mvc.perform(post("/user/showUser").param("id", licznik_przed + "")).andExpect(status().isOk())
////                .andExpect(jsonPath("$.id").value(user.getId())).andExpect(jsonPath("$.username").value(user.getUsername()))
////                .andExpect(jsonPath("$.password").value(user.getPassword())).andExpect(jsonPath("$.money").value(user.getMoney()));
//    }
//
//
//    @Test
//    public void deleteLastUser() throws Exception {
////        long licznik_przed = userRepository.count();
////        ObjectMapper objectMapper = new ObjectMapper();
////        this.mvc.perform(deleteById("/user/deleteById").param("id", licznik_przed + "")).andExpect(status().isOk())
////                .andExpect(content().contentType("application/json;charset=UTF-8"))
////                .andExpect(content().string(licznik_przed + ""));
////        long licznik_po = userRepository.count();
////        Assert.assertEquals(licznik_przed - 1, licznik_po);
//    }
//
//
//
//
//
//    @Test
//    public void create() throws Exception {
////        long licznik_przed = courseRepository.count();
//        CourseCategory course_category = new CourseCategory();
//        course_category.setId(1);
//        course_category.setName("Informatyka");
//
////        CoursePacket course = new CoursePacket(1, "Kurs Ruby", "Wprowadzenie do jezyka programowania", 3, "", 0, 3, "...", course_category);
////        course_category.setCourseSet(new HashSet<CoursePacket>(){{add(course);}});
//
//        ObjectMapper objectMapper = new ObjectMapper();
////        this.mvc.perform(post("/api/user/create").content(objectMapper.writeValueAsString(course)).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
////                .andExpect(content().contentType("application/json;charset=UTF-8"))
////                .andExpect(jsonPath("$.id").value(course.getId()));
//        long licznik_po = courseRepository.count();
////        Assert.assertEquals(licznik_przed + 1, licznik_po);
//    }
//


}