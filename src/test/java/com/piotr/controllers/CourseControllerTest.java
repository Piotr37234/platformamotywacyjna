package com.piotr.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.piotr.DTO.*;
import com.piotr.entities.*;
import com.piotr.main.PlatformaMotywacyjnaApplicationTests;
import com.piotr.repository.*;
import com.piotr.tools.CourseCategoryUserTools;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CourseControllerTest extends PlatformaMotywacyjnaApplicationTests {

    CourseCategoryUserTools categoryUserTools = new CourseCategoryUserTools();

    @Autowired
    private CourseController courseController;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CourseCategoryRepository courseCategoryRepository;

    @Autowired
    private LessonCourseRepository lessonCourseRepository;

    @Autowired
    private AnswerRepository answerRepository;

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private ElementLessonRepository elementLessonRepository;

    @Autowired
    private LessonUserRepository lessonUserRepository;


    @Test
    public void MockMvcTest() throws Exception {
        Assert.assertNotNull(this.mvc);
    }

    @Transactional
    @Test
    public void create() throws Exception {
        int przed = (int) courseRepository.count();
        CourseCategory courseCategory = new CourseCategory(900, "xyz", null);
        courseCategoryRepository.save(courseCategory);
        courseCategory = courseCategoryRepository.findByName("xyz");
        User user = new User(900, "xyz", "haslo", 122, "image.png", 1, 120, 23, 32, 1, 32, null);
        userRepository.save(user);
        user = userRepository.findByUsername("xyz");
        CategoryPacket categoryPacket = new CategoryPacket(courseCategory.getId(), "xyz");
        UserPacket userPacket = new UserPacket(user.getId(), "xyz", "haslo", 34, "image.png", 12, 23, 42, 23, 3, 2);
        CoursePacket coursePacket = new CoursePacket(900, "xyz", "opis", 32, "haslo", 32, 1, "image.png", categoryPacket, userPacket, null);
        ObjectMapper objectMapper = new ObjectMapper();
        this.mvc.perform(post("/api/course/update")
                .content(objectMapper.writeValueAsString(coursePacket))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"));
        int po = (int) courseRepository.count();
        Assert.assertEquals(przed+1, po);
    }


    @Transactional
    @Test
    public void update() throws Exception {
        CourseCategory courseCategory = new CourseCategory(900, "xyz", null);
        courseCategoryRepository.save(courseCategory);
        courseCategory = courseCategoryRepository.findByName("xyz");

        User user = new User(900, "xyz", "haslo", 122, "image.png", 1, 120, 23, 32, 1, 32, null);
        userRepository.save(user);
        user = userRepository.findByUsername("xyz");

        Course course = new Course(900, "xyz", "opis", 32, "haslo", 32, 1, "image.png", courseCategory, user, null);
        courseRepository.save(course);
        course = courseRepository.findByName("xyz");

        CategoryPacket categoryPacket = new CategoryPacket(courseCategory.getId(), "xyz");
        UserPacket userPacket = new UserPacket(user.getId(), "xyz", "haslo", 34, "image.png", 12, 23, 42, 23, 3, 2);
        CoursePacket coursePacket = new CoursePacket(course.getId(), "xyz", "opis", 32, "haslo", 32, 1, "image.png", categoryPacket, userPacket, null);


        ObjectMapper objectMapper = new ObjectMapper();
        this.mvc.perform(post("/api/course/update")
                .content(objectMapper.writeValueAsString(coursePacket))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"));
    }

    @Transactional
    @Test
    public void getCourse() throws Exception {
        CourseCategory courseCategory = new CourseCategory(900, "xyz", null);
        courseCategoryRepository.save(courseCategory);
        courseCategory = courseCategoryRepository.findByName("xyz");

        User user = new User(900, "xyz", "haslo", 122, "image.png", 1, 120, 23, 32, 1, 32, null);
        userRepository.save(user);
        user = userRepository.findByUsername("xyz");

        Course course = new Course(900, "xyz", "opis", 32, "haslo", 32, 1, "image.png", courseCategory, user, null);

        courseRepository.save(course);
        course = courseRepository.findByName("xyz");

        this.mvc.perform(get("/api/course/get/" + course.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"));
    }


    @Transactional
    @Test
    public void deletebyId() throws Exception {
        Course course = new Course(900, "xyz", "opis", 32, "haslo", 32, 1, "image.png", null, null, null);
        courseController.curseSave(course);
        course = courseRepository.findByName("xyz");
        this.mvc.perform(delete("/api/course/delete/" + course.getId()))
                .andExpect(status().isOk());
        course = courseRepository.findByName("xyz");
        Assert.assertNull(course);
    }

    @Transactional
    @Test
    public void deleteAll() throws Exception {
        this.mvc.perform(delete("/api/course/deleteAllCourses"))
                .andExpect(status().isOk());
    }

    @Transactional
    @Test
    public void getAll() throws Exception {
        this.mvc.perform(get("/api/course/getAll"))
                .andExpect(status().isOk());
    }


    @Transactional
    @Test
    public void getAllCategory() throws Exception {
        this.mvc.perform(get("/api/course/getAllCategory"))
                .andExpect(status().isOk());
    }

    @Transactional
    @Test
    public void curseSave() {
        int przed = (int) courseRepository.count();

        Course course = new Course(przed + 1, "xyz", "opis", 32, "haslo", 32, 1, "image.png", null, null, null);
        courseController.curseSave(course);
        int poDodaniu = (int) courseRepository.count();
        Assert.assertEquals(przed + 1, poDodaniu);

        course = courseRepository.findByName("xyz");

        courseController.curseDelete(course);
        int poUsunieciu = (int) courseRepository.count();
        Assert.assertEquals(przed, poUsunieciu);

    }


    @Transactional
    @Test
    public void lessonCurseSave() {
        int przed = (int) lessonCourseRepository.count();
        LessonCourse lessonCourse = new LessonCourse(przed + 1, "xyz", 323, 1, 0, -1, null, null, null);

        courseController.lessonCurseSave(lessonCourse);
        int poDodaniu = (int) lessonCourseRepository.count();
        Assert.assertEquals(przed + 1, poDodaniu);

        lessonCourse = lessonCourseRepository.findByName("xyz");
        courseController.lessonCurseDelete(lessonCourse);
        int poUsunieciu = (int) lessonCourseRepository.count();

        Assert.assertEquals(przed, poUsunieciu);
    }

    @Transactional
    @Test
    public void elementLessonSaveAndDelete() {
        int przed = (int) elementLessonRepository.count();
        ElementLesson elementLesson = new ElementLesson(przed + 1, "xyz", "text", "text", 1, null, null);

        courseController.elementLessonSave(elementLesson);
        int poDodaniu = (int) elementLessonRepository.count();
        Assert.assertEquals(przed + 1, poDodaniu);

        elementLesson = elementLessonRepository.findByName("xyz");
        courseController.elementLessonDelete(elementLesson);
        int poUsunieciu = (int) elementLessonRepository.count();
        Assert.assertEquals(przed, poUsunieciu);
    }

    @Transactional
    @Test
    public void questionSaveAndDelete() {
        int przed = (int) questionRepository.count();
        Question question = new Question(przed + 1, "xyz", 12, null, null);

        courseController.questionSave(question);
        int poDodaniu = (int) questionRepository.count();
        Assert.assertEquals(przed + 1, poDodaniu);

        question = questionRepository.findByContent("xyz");
        courseController.questionDelete(question);
        int poUsunieciu = (int) elementLessonRepository.count();

    }


    @Transactional
    @Test
    public void saveUserToCourse() {
        LessonCourse lessonCourse = new LessonCourse(900, "xyzc", 323, 1, 0, -1, null, null, null);
        lessonCourseRepository.save(lessonCourse);
        lessonCourse = lessonCourseRepository.findByName("xyzc");

        List<LessonCourse> list = new ArrayList<>();
        list.add(lessonCourse);
        Course course = new Course(900, "xyzc", "opis", 32, "haslo", 32, 1, "image.png", null, null, list);
        User user = new User(900, "xyzc", "haslo", 122, "image.png", 1, 120, 23, 32, 1, 32, null);

        courseRepository.save(course);
        userRepository.save(user);

        course = courseRepository.findByName("xyzc");
        user = userRepository.findByUsername("xyzc");

        SaveUserCoursesPacket saveUserCoursesPacket = new SaveUserCoursesPacket(user.getId(), course.getId());
        courseController.saveUserToCourse(saveUserCoursesPacket);
    }

    @Transactional
    @Test
    public void modifyLesson() {
        User user = new User(900, "xyz", "haslo", 122, "image.png", 1, 120, 23, 32, 1, 32, null);
        userRepository.save(user);
        user = userRepository.findByUsername("xyz");

        LessonCourse lessonCourse = new LessonCourse(900, "xyz", 323, 1, 0, -1, null, null, null);
        lessonCourseRepository.save(lessonCourse);
        lessonCourse = lessonCourseRepository.findByName("xyz");

        LessonUser lessonUser = new LessonUser(900, 0, 0, 0, 0, 0, 0, 0, lessonCourse, user);
        lessonUserRepository.save(lessonUser);
        lessonUser = lessonUserRepository.findByUserAndLessonCurse(user, lessonCourse);

        int Pcompleted = lessonUser.getUnlocked();
        int Punlock = lessonUser.getUnlocked();
        int PcorrectQuestion = lessonUser.getCorrectQuestion();
        Assert.assertEquals(0, Pcompleted);
        Assert.assertEquals(0, Punlock);
        Assert.assertEquals(0, PcorrectQuestion);

        ModifyLessonUserPacket modifyLessonUserPacket = new ModifyLessonUserPacket(user.getId(), lessonCourse.getId(), 1, 1, 12, -1, -1, -1);
        courseController.modifyLesson(modifyLessonUserPacket);

        lessonUser = lessonUserRepository.findByUserAndLessonCurse(user, lessonCourse);
        int completed = lessonUser.getUnlocked();
        int unlock = lessonUser.getUnlocked();
        int correctQuestion = lessonUser.getCorrectQuestion();

        Assert.assertEquals(1, completed);
        Assert.assertEquals(1, unlock);
        Assert.assertEquals(12, correctQuestion);

    }

    @Transactional
    @Test
    public void createCategory(){
        CategoryPacket categoryPacket = new CategoryPacket(900,"xyz");
        courseController.createCategory(categoryPacket);
        CourseCategory courseCategory = courseCategoryRepository.findByName("xyz");
        Assert.assertNotNull(courseCategory);
    }
}