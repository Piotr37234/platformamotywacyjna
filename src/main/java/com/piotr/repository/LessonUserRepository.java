package com.piotr.repository;

import com.piotr.entities.Course;
import com.piotr.entities.LessonCourse;
import com.piotr.entities.LessonUser;
import com.piotr.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface LessonUserRepository extends JpaRepository<LessonUser,Integer> {
    LessonUser findByUserAndLessonCurse(User user, LessonCourse lessonCourse);
}
