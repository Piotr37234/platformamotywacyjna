package com.piotr.repository;

import com.piotr.entities.Answer;
import com.piotr.entities.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface QuestionRepository extends JpaRepository<Question,Integer> {
    Question findByContent(String content);
}
