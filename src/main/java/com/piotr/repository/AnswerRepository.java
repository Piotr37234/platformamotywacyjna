package com.piotr.repository;

import com.piotr.entities.Answer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface AnswerRepository extends JpaRepository<Answer, Integer> {
//    HashSet<Answer> findByQuestions_id(int id);
//    HashSet<Answer> findByContent(String content);
    Answer findByGood(boolean good);
    Answer findByContent(String content);

}
