package com.piotr.repository;

import com.piotr.entities.Course;
import com.piotr.entities.ElementLesson;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CourseRepository extends JpaRepository<Course,Integer> {
    Course findByName(String name);
}
