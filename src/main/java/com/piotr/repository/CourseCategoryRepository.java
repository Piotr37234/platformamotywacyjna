package com.piotr.repository;

import com.piotr.entities.Course;
import com.piotr.entities.CourseCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CourseCategoryRepository extends JpaRepository<CourseCategory,Integer> {
    CourseCategory findByName(String name);
}
