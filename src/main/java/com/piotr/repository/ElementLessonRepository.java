package com.piotr.repository;

import com.piotr.entities.Answer;
import com.piotr.entities.ElementLesson;
import com.piotr.entities.LessonCourse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ElementLessonRepository extends JpaRepository<ElementLesson,Integer> {
    ElementLesson findByName(String name);
}
