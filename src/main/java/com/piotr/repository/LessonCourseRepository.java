package com.piotr.repository;

import com.piotr.entities.LessonCourse;
import com.piotr.entities.LessonUser;
import com.piotr.entities.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface LessonCourseRepository extends JpaRepository<LessonCourse,Integer> {
    LessonCourse findByName(String name);
}
