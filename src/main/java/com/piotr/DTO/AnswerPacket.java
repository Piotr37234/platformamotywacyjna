package com.piotr.DTO;

import com.piotr.entities.Question;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AnswerPacket {
    private int id;
    private String content;
    private boolean good;
    private boolean odp;
}
