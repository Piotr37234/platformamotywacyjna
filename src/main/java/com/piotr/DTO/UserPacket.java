package com.piotr.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserPacket {
    private int id;
    private String username;
    private String password;
    private double money;
    private String img;
    private int completedCourse;
    private int errorAnswer;
    private int correctAnswer;
    private int effectiveness;
    private int createCurses;
    private int experience;
}
