package com.piotr.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class ElementLessonPacket {
    @Id
    private int id;
    private String name;
    private String header;
    private String content;
    private int type;
    private List<QuestionPacket> questionPackets = new ArrayList<>();
}
