package com.piotr.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuestionPacket {
    private int id;
    private String content;
    private int pkt;
    private List<AnswerPacket> answerPackets = new ArrayList<>();
}

