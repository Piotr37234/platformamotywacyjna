package com.piotr.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ModifyLessonUserPacket {
    private int idUser;
    private int idLesson;
    private int unlock;
    private int completed;
    private int correctQuestion;
    private int errorQuestion;
    private int trials;
    private int result;

}

