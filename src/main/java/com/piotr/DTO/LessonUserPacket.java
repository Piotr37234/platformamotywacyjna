package com.piotr.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class LessonUserPacket {
    @Id
    private int id;
    private int completed;
    private int result;
    private int unlocked;
    private int type;
    private int correctQuestion;
    private int errorQuestion;
    private int trials;
    private String username;
}
