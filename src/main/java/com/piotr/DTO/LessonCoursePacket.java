package com.piotr.DTO;

import com.piotr.entities.ElementLesson;
import com.piotr.entities.LessonUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class LessonCoursePacket {
    @Id
    private int id;
    private String name;
    private int cost;
    private int type;
    private int locked;
    private int idNext;

    private List<ElementLessonPacket> elementLessons = new ArrayList<>();
    private List<LessonUserPacket> lessonUsers = new ArrayList<>();
}
