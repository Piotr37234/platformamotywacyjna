package com.piotr.DTO;

import com.piotr.entities.CourseCategory;
import com.piotr.entities.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CoursePacket {
    private int id;
    private String name;
    private String description;
    private int rate;
    private String password;
    private int cost;
    private int rank;
    private String img;
    private CategoryPacket category;
    private UserPacket autor;
    private List<LessonCoursePacket> lessonCoursePackets = new ArrayList<>();
}
