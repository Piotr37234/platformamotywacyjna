package com.piotr.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan({"com.piotr"})
@EntityScan("com.piotr.entities")
@EnableJpaRepositories("com.piotr.repository")
public class PlatformaMotywacyjnaApplication {
	public static void main(String[] args) {
		SpringApplication.run(PlatformaMotywacyjnaApplication.class, args);
	}
}
