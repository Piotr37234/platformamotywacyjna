package com.piotr.tools;

import com.piotr.DTO.AnswerPacket;
import com.piotr.DTO.QuestionPacket;
import com.piotr.entities.Answer;
import com.piotr.entities.Question;

public class QuestionAnswerTools {
    public QuestionPacket mapQuestionToPacket(Question question) {
        QuestionPacket questionPacket = new QuestionPacket();
        questionPacket.setId(question.getId());
        questionPacket.setContent(question.getContent());
        questionPacket.setPkt(question.getPkt());
        for (Answer answer : question.getAnswers()) {
            AnswerPacket answerPacket = mapAnswerToPacket(answer);
            questionPacket.getAnswerPackets().add(answerPacket);
        }
        return questionPacket;
    }

    public AnswerPacket mapAnswerToPacket(Answer answer) {
        AnswerPacket answerPacket = new AnswerPacket();
        answerPacket.setId(answer.getId());
        answerPacket.setContent(answer.getContent());
        answerPacket.setGood(answer.isGood());
        answerPacket.setOdp(answer.isOdp());
        return answerPacket;
    }

    public Answer mapPacketToAnswer(AnswerPacket answerPacket) {
        Answer answer = new Answer();
        answer.setId(answerPacket.getId());
        answer.setContent(answerPacket.getContent());
        answer.setGood(answerPacket.isGood());
        answer.setOdp(answerPacket.isOdp());
        return answer;
    }

    public Question mapPacketToQuestion(QuestionPacket questionPacket) {
        Question question = new Question();
        question.setId(questionPacket.getId());
        question.setContent(questionPacket.getContent());
        question.setPkt(questionPacket.getPkt());

        for (AnswerPacket answerPacket : questionPacket.getAnswerPackets()) {
            Answer answer = mapPacketToAnswer(answerPacket);
            question.getAnswers().add(answer);
        }
        return question;
    }
}
