package com.piotr.tools;

import com.piotr.DTO.ElementLessonPacket;
import com.piotr.DTO.LessonCoursePacket;
import com.piotr.DTO.LessonUserPacket;
import com.piotr.DTO.QuestionPacket;
import com.piotr.entities.*;
import com.piotr.repository.UserRepository;

public class LessonTools {

    QuestionAnswerTools questionAnswerTools = new QuestionAnswerTools();

    public ElementLessonPacket mapElementLessonToPacket(ElementLesson elementLesson) {
        ElementLessonPacket elementLessonPacket = new ElementLessonPacket();

        elementLessonPacket.setId(elementLesson.getId());
        elementLessonPacket.setContent(elementLesson.getContent());
        elementLessonPacket.setHeader(elementLesson.getHeader());
        elementLessonPacket.setName(elementLesson.getName());
        elementLessonPacket.setType(elementLesson.getType());

        for (Question question : elementLesson.getQuestions()) {
            QuestionPacket questionPacket = questionAnswerTools.mapQuestionToPacket(question);
            elementLessonPacket.getQuestionPackets().add(questionPacket);
        }
        return elementLessonPacket;
    }

    public ElementLesson mapPacketToElementLesson(ElementLessonPacket elementLessonPacket) {
        ElementLesson elementLesson = new ElementLesson();

        elementLesson.setId(elementLessonPacket.getId());
        elementLesson.setContent(elementLessonPacket.getContent());
        elementLesson.setHeader(elementLessonPacket.getHeader());
        elementLesson.setName(elementLessonPacket.getName());
        elementLesson.setType(elementLessonPacket.getType());

        for (QuestionPacket questionPacket : elementLessonPacket.getQuestionPackets()) {
            Question question = questionAnswerTools.mapPacketToQuestion(questionPacket);
            elementLesson.getQuestions().add(question);
        }
        return elementLesson;
    }

    public LessonCoursePacket mapLessonCourseToPacket(LessonCourse lessonCourse) {
        LessonCoursePacket lessonCoursePacket = new LessonCoursePacket();

        lessonCoursePacket.setId(lessonCourse.getId());
        lessonCoursePacket.setCost(lessonCourse.getCost());
        lessonCoursePacket.setName(lessonCourse.getName());
        lessonCoursePacket.setType(lessonCourse.getType());
        lessonCoursePacket.setLocked(lessonCourse.getLocked());
        lessonCoursePacket.setIdNext(lessonCourse.getIdNext());

        for (ElementLesson elementLesson : lessonCourse.getElementLessons()) {
            ElementLessonPacket elementLessonPacket = mapElementLessonToPacket(elementLesson);
            lessonCoursePacket.getElementLessons().add(elementLessonPacket);
        }

        for (LessonUser lessonUser : lessonCourse.getLessonUser()) {
            LessonUserPacket lessonUserPacket = mapLessonUserToPacket(lessonUser);
            lessonCoursePacket.getLessonUsers().add(lessonUserPacket);
        }

        return lessonCoursePacket;
    }

    public LessonCourse mapPacketToLessonCourse(LessonCoursePacket lessonCoursePacket,UserRepository userRepository) {
        LessonCourse lessonCourse = new LessonCourse();
        lessonCourse.setId(lessonCoursePacket.getId());
        lessonCourse.setCost(lessonCoursePacket.getCost());
        lessonCourse.setName(lessonCoursePacket.getName());
        lessonCourse.setType(lessonCoursePacket.getType());
        lessonCourse.setLocked(lessonCoursePacket.getLocked());
        lessonCourse.setIdNext(lessonCoursePacket.getIdNext());

        for (ElementLessonPacket elementLessonPacket : lessonCoursePacket.getElementLessons()) {
            ElementLesson elementLesson = mapPacketToElementLesson(elementLessonPacket);
            lessonCourse.getElementLessons().add(elementLesson);
        }

        for (LessonUserPacket lessonUserPacket : lessonCoursePacket.getLessonUsers()) {
            LessonUser lessonUser = mapPacketToLessonUser(lessonUserPacket,userRepository);
            lessonCourse.getLessonUser().add(lessonUser);
        }
        return lessonCourse;
    }


    public LessonUserPacket mapLessonUserToPacket(LessonUser lessonUser) {
        LessonUserPacket lessonUserPacket = new LessonUserPacket();
        lessonUserPacket.setId(lessonUser.getId());
        lessonUserPacket.setType(lessonUser.getType());
        lessonUserPacket.setUsername(lessonUser.getUser().getUsername());
        lessonUserPacket.setCompleted(lessonUser.getCompleted());
        lessonUserPacket.setResult(lessonUser.getResult());
        lessonUserPacket.setUnlocked(lessonUser.getUnlocked());
        lessonUserPacket.setCorrectQuestion(lessonUser.getCorrectQuestion());
        lessonUserPacket.setErrorQuestion(lessonUser.getErrorQuestion());
        lessonUserPacket.setTrials(lessonUser.getTrials());

//        LessonCoursePacket lessonCoursePacket = mapLessonCourseToPacket(lessonUser.getLessonCurse());
//        lessonUserPacket.setLessonCoursePacket(lessonCoursePacket);

        return lessonUserPacket;
    }

    public LessonUser mapPacketToLessonUser(LessonUserPacket lessonUserPacket, UserRepository userRepository) {
        LessonUser lessonUser = new LessonUser();
        lessonUser.setId(lessonUserPacket.getId());
        lessonUser.setType(lessonUserPacket.getType());

        User user = userRepository.findByUsername(lessonUserPacket.getUsername());
        lessonUser.setUser(user);

        lessonUser.setCompleted(lessonUserPacket.getCompleted());
        lessonUser.setResult(lessonUserPacket.getResult());
        lessonUser.setUnlocked(lessonUserPacket.getUnlocked());
        lessonUser.setCorrectQuestion(lessonUserPacket.getCorrectQuestion());
        lessonUser.setErrorQuestion(lessonUserPacket.getErrorQuestion());
        lessonUser.setTrials(lessonUserPacket.getTrials());


//        LessonCourse lessonCourse = mapPacketToLessonCourse(lessonUserPacket.getLessonCoursePacket());
//        lessonUser.setLessonCurse(lessonCourse);

        return lessonUser;
    }

}
