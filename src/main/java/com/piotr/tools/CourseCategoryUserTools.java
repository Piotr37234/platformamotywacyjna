package com.piotr.tools;

import com.piotr.DTO.*;
import com.piotr.entities.*;
import com.piotr.repository.UserRepository;

import java.util.Locale;

public class CourseCategoryUserTools {

    LessonTools lessonTools = new LessonTools();


    public CoursePacket mapCourseToPacket(Course course) {
        CoursePacket coursePacket = new CoursePacket();
        coursePacket.setId(course.getId());

        UserPacket userPacket = mapUserToPacket(course.getAutor());
        coursePacket.setAutor(userPacket);

        CategoryPacket categoryPacket = mapCategoryToPacket(course.getCategory());
        coursePacket.setCategory(categoryPacket);


        coursePacket.setCost(course.getCost());
        coursePacket.setDescription(course.getDescription());
        coursePacket.setImg(course.getImg());
        coursePacket.setName(course.getName());
        coursePacket.setPassword(course.getPassword());
        coursePacket.setRank(course.getRank());
        coursePacket.setRate(course.getRate());


        if (course.getLessonCourses() != null) {
            for (LessonCourse lessonCourse : course.getLessonCourses()) {
                LessonCoursePacket lessonCoursePacket = lessonTools.mapLessonCourseToPacket(lessonCourse);
                coursePacket.getLessonCoursePackets().add(lessonCoursePacket);
            }
        }

        return coursePacket;
    }

    public Course mapPacketToCourse(CoursePacket coursePacket, UserRepository userRepository) {
        Course course = new Course();
        course.setId(coursePacket.getId());

        User user = mapPacketToUser(coursePacket.getAutor());
        course.setAutor(user);


        CourseCategory courseCategory = mapPacketToCategory(coursePacket.getCategory());
        course.setCategory(courseCategory);

        System.err.println("course" + course.toString());
        course.setCost(coursePacket.getCost());
        course.setDescription(coursePacket.getDescription());
        course.setImg(coursePacket.getImg());
        course.setName(coursePacket.getName());
        course.setPassword(coursePacket.getPassword());
        course.setRank(coursePacket.getRank());
        course.setRate(coursePacket.getRate());


        if (coursePacket.getLessonCoursePackets()!= null) {
            System.err.println("1coursewynikowe" + course.toString());
            for (LessonCoursePacket lessonCoursePacket : coursePacket.getLessonCoursePackets()) {
                LessonCourse lessonCourse = lessonTools.mapPacketToLessonCourse(lessonCoursePacket, userRepository);
                System.err.println("2coursewynikowe" + course.toString());
                course.getLessonCourses().add(lessonCourse);
                System.err.println("3coursewynikowe" + course.toString());
            }
        }

        System.err.println("coursewynikowe" + course.toString());
        return course;
    }

    public User mapPacketToUser(UserPacket userPacket) {
        User user = new User();
        user.setId(userPacket.getId());
        user.setMoney(userPacket.getMoney());
        user.setPassword(userPacket.getPassword());
        user.setUsername(userPacket.getUsername());
        user.setImg(userPacket.getImg());
        user.setCompletedCourse(userPacket.getCompletedCourse());
        user.setCorrectAnswer(userPacket.getCorrectAnswer());
        user.setCreateCurses(userPacket.getCreateCurses());
        user.setEffectiveness(userPacket.getEffectiveness());
        user.setErrorAnswer(userPacket.getErrorAnswer());
        user.setExperience(userPacket.getExperience());
        return user;
    }

    public UserPacket mapUserToPacket(User user) {
        UserPacket userPacket = new UserPacket();
        userPacket.setId(user.getId());
        userPacket.setMoney(user.getMoney());
        userPacket.setPassword(user.getPassword());
        userPacket.setUsername(user.getUsername());
        userPacket.setImg(user.getImg());
        userPacket.setCompletedCourse(user.getCompletedCourse());
        userPacket.setCorrectAnswer(user.getCorrectAnswer());
        userPacket.setCreateCurses(user.getCreateCurses());
        userPacket.setEffectiveness(user.getEffectiveness());
        userPacket.setErrorAnswer(user.getErrorAnswer());
        userPacket.setExperience(user.getExperience());
        return userPacket;
    }

    public CategoryPacket mapCategoryToPacket(CourseCategory courseCategory) {
        CategoryPacket categoryPacket = new CategoryPacket();
        categoryPacket.setId(courseCategory.getId());
        categoryPacket.setName(courseCategory.getName());
        return categoryPacket;
    }

    public CourseCategory mapPacketToCategory(CategoryPacket categoryPacket) {
        CourseCategory courseCategory = new CourseCategory();
        courseCategory.setId(categoryPacket.getId());
        courseCategory.setName(categoryPacket.getName());
//        System.err.println(courseCategory.toString());
        return courseCategory;
    }


}
