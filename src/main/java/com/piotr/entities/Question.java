package com.piotr.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Question {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;
    private String content;
    private int pkt;

    @ManyToMany
    private List<Answer> answers = new ArrayList<>();

    @ManyToMany(mappedBy = "questions")
    private List<ElementLesson> elementLessons = new ArrayList<>();


//    public Optional<Question> mappedToEntity(QuestionDTO questionDTO, QuestionRepository questionRepository) {
//        id = questionDTO.getId();
//        content = questionDTO.getContent();
//
//        return this;
//    }
//
//    public Optional<QuestionDTO> mappedToDTO(Question question, QuestionRepository questionRepository) {
//        QuestionDTO questionDTO = new QuestionDTO();
//        questionDTO.setId(id);
//        questionDTO.setContent(content);
//
//        Set<AnswerPacket> answersDTO = new HashSet<>();
//        for(Answer a: answers)
//            answersDTO.add(a.)
//
//
//        questionDTO.setAnswers();
//        return this;
//    }


}
//    @ManyToMany(mappedBy = "userGroups", fetch = FetchType.EAGER)
//    private Set<User> users = new HashSet<User>();
////export interface Question {
//    id: number;
//    elementLesson: ElementLesson;
//    questionContent: QuestionContent;
//}
//@JoinTable(name = "QuestionAnswer" ,joinColumns = @JoinColumn(name = "Answer_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "Question_id", referencedColumnName = "id"))

