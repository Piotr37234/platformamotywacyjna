package com.piotr.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LessonCourse {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;
    private String name;
    private int cost;
    private int type;
    private int locked;
    private int idNext;


//    @ManyToOne
//    @JoinColumn(name="course")
//    private Course course;
//
    @ManyToMany(mappedBy = "lessonCourses")
    private List<Course> courses = new ArrayList<>();

//    @ManyToOne(na)
//    private List<Course> courses = new ArrayList<>();

    @OneToMany(mappedBy = "lessonCurse", cascade = CascadeType.ALL)
    private List<LessonUser> lessonUser = new ArrayList<>();

//    @ManyToMany
//    private List<LessonUser> lessonUser = new ArrayList<>();

    @ManyToMany
    private List<ElementLesson> elementLessons = new ArrayList<>();

}

//id: number;
//        name: string;
//        cost: number;