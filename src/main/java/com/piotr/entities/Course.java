package com.piotr.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String description;
    private int rate;
    private String password;
    private int cost;
    private int rank;
    private String img;

    @ManyToOne
    @JoinColumn(name = "category")
    private CourseCategory category;
    //        @OneToMany(mappedBy = "company", cascade = CascadeType.ALL, fetch = FetchType.EAGER)

    @ManyToOne
    @JoinColumn(name = "autor")
    private User autor;

    @ManyToMany
    private List<LessonCourse> lessonCourses = new ArrayList<>();


}
//    @OneToMany(mappedBy = "course", cascade = CascadeType.ALL)
//    private List<LessonCourse> lessonCourses = new ArrayList<>();
//
//    @OneToMany(mappedBy = "courses", cascade = CascadeType.ALL)
//    private List<LessonCourse> lessonCourses = new ArrayList<>();



//    public Course mapped(CoursePacket courseDTO, CourseCategoryRepository course_categoryRepository, UserRepository userRepository){
//        id = courseDTO.getId();
//        name = courseDTO.getName();
//        description = courseDTO.getDescription();
//        rate = courseDTO.getRate();
//        password = courseDTO.getPassword();
//        cost = courseDTO.getCost();
//        rank = courseDTO.getRank();
//        img = courseDTO.getImg();
//        course_category = course_categoryRepository.getOne(courseDTO.getCourse_category());
//        user = userRepository.getOne(courseDTO.getUser());
//        return this;
//    }

