package com.piotr.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LessonUser {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;
    private int completed;
    private int result;
    private int unlocked;
    private int type;
    private int correctQuestion;
    private int errorQuestion;
    private int trials;

    @ManyToOne
    @JoinColumn(name="lessonCurse")
    private LessonCourse lessonCurse;
//
//    @ManyToMany(mappedBy = "lessonUser")
//    private LessonCourse lessonCurse;

    @ManyToOne
    @JoinColumn(name="user")
    private User user;
}
