package com.piotr.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;
    private String username;
    private String password;
    private double money;
    private String img;
    private int completedCourse;
    private int errorAnswer;
    private int correctAnswer;
    private int effectiveness;
    private int createCurses;
    private int experience;

    @OneToMany(mappedBy = "autor", cascade = CascadeType.ALL)
    private List<Course> courses = new ArrayList<>();
}
