package com.piotr.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ElementLesson {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;
    private String name;
    private String header;
    private String content;
    private int type;

    @ManyToMany
    private List<Question> questions = new ArrayList<>();

    @ManyToMany(mappedBy = "elementLessons")
    private List<LessonCourse> lessonCourseList = new ArrayList<>();


}