package com.piotr.entities;

import com.piotr.DTO.AnswerPacket;
import com.piotr.repository.AnswerRepository;
import com.piotr.repository.QuestionRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Answer {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;
    private String content;
    private boolean good;
    private boolean odp;

    @ManyToMany(mappedBy = "answers")
    private List<Question> questions = new ArrayList<>();

//    public Optional<Answer> mapToEntity(Optional<AnswerPacket> answerDTO, AnswerRepository answerRepository, QuestionRepository questionRepository) {
//        id = answerDTO.getUser().getId();
//        content = answerDTO.getUser().getContent();
//        good = answerDTO.getUser().getIsGood();
//        questions.add(questionRepository.getOne(answerDTO.getUser().getQuestion()));
//        return Optional.of(this);
//    }
//
//    public Optional<AnswerPacket> mapToDTO(Optional<Answer> answer, AnswerRepository answerRepository, QuestionRepository questionRepository) {
//        AnswerPacket answerDTO = new AnswerPacket();
//        answerDTO.setId(id);
//        answerDTO.setContent(content);
//        answerDTO.setIsGood(good);
//
//        answerDTO.setQuestion();
//
//        id = answerDTO.getId();
//        content = answerDTO.getContent();
//        good = answerDTO.getIsGood();
//        questions.add(questionRepository.getOne(answerDTO.getQuestion()));
//        return this;
//    }


}

