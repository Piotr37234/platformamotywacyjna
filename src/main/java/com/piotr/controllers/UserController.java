package com.piotr.controllers;

import com.piotr.DTO.*;
import com.piotr.entities.CourseCategory;
import com.piotr.entities.Question;
import com.piotr.entities.User;
import com.piotr.repository.UserRepository;
import com.piotr.tools.CourseCategoryUserTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserController {

    CourseCategoryUserTools courseCategoryUserTools = new CourseCategoryUserTools();

    @Autowired
    private UserRepository userRepository;

    @GetMapping("hello")
    public String hello() {
        return "czesc :)";
    }

    @PostMapping("create")
    public User registry(@RequestBody UserPacket userPacket) {
        User user = courseCategoryUserTools.mapPacketToUser(userPacket);
        userRepository.save(user);
        return user;
    }

    @DeleteMapping("delete/{id}")
    public long deleteUser(@PathVariable("id") int id) {
        userRepository.deleteById(id);
        return id;
    }

    @GetMapping("get/{id}")
    public UserPacket getUser(@PathVariable("id") int id) {
        User user = userRepository.findById(id).get();
        UserPacket userPacket = courseCategoryUserTools.mapUserToPacket(user);
        return userPacket;
    }

    @GetMapping("showAllUsers")
    public List<UserPacket> showAllUsers() {
        List<User> users = userRepository.findAll();
        List<UserPacket> userPackets = new ArrayList<>();
        for(User user : users){
            UserPacket userPacket = courseCategoryUserTools.mapUserToPacket(user);
            userPackets.add(userPacket);
        }
        return userPackets;
    }



    @PostMapping("getAccountUser")
    public UserPacket getAccountUser(@RequestBody LoginPacket packet) {
        User user = userRepository.findByUsername(packet.getUsername());
        if (user != null && user.getPassword().equals(packet.getPassword())) {
            UserPacket userPacket = courseCategoryUserTools.mapUserToPacket(user);
            return userPacket;
        } else {
            return null;
        }
    }

    @PostMapping("addMoney")
    public double addMoney(@RequestBody TransferMoneyPacket transferMoneyPacket) {
        User user = userRepository.findById(transferMoneyPacket.getIdUser()).get();
        user.setMoney(user.getMoney() + transferMoneyPacket.getValue());
        userRepository.save(user);
        return user.getMoney();
    }
}
