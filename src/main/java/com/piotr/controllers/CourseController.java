package com.piotr.controllers;

import com.piotr.DTO.*;
import com.piotr.entities.*;
import com.piotr.repository.*;
import com.piotr.tools.CourseCategoryUserTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/course")
public class CourseController {

    CourseCategoryUserTools courseTools = new CourseCategoryUserTools();

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CourseCategoryRepository courseCategoryRepository;

    @Autowired
    private LessonCourseRepository lessonCourseRepository;

    @Autowired
    private AnswerRepository answerRepository;

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private ElementLessonRepository elementLessonRepository;

    @Autowired
    private LessonUserRepository lessonUserRepository;


    @PostMapping("create")
    public CoursePacket create(@RequestBody CoursePacket coursePacket) {
        Course course = courseTools.mapPacketToCourse(coursePacket, userRepository);
        curseSave(course);
        course  = courseRepository.findByName(course.getName());
        CoursePacket coursePacket1 = courseTools.mapCourseToPacket(course);
        return coursePacket1;
    }

    @PostMapping("update")
    public CoursePacket update(@RequestBody CoursePacket coursePacket) {
        if (courseRepository.existsById(coursePacket.getId())) {
            System.err.println("Przyslane pytnia"+coursePacket.getLessonCoursePackets().get(0).getElementLessons().get(0).getQuestionPackets().toString());
            Course course = courseTools.mapPacketToCourse(coursePacket, userRepository);
            System.err.println(course.toString());
            curseSave(course);
            return coursePacket;
        }
        return null;
    }

    @GetMapping("get/{id}")
    public CoursePacket getCourse(@PathVariable("id") int id) {
        Course course = courseRepository.findById(id).get();
        CoursePacket coursePacket = courseTools.mapCourseToPacket(course);
        return coursePacket;
    }

    @DeleteMapping("delete/{id}")
    public int deleteById(@PathVariable("id") int id) {
        Course course = courseRepository.findById(id).get();
        curseDelete(course);
        return 1;
    }

    @DeleteMapping("deleteAllCourses")
    public int deleteAll() {
        courseRepository.deleteAll();
        courseCategoryRepository.deleteAll();
        lessonCourseRepository.deleteAll();
        answerRepository.deleteAll();
        questionRepository.deleteAll();
        elementLessonRepository.deleteAll();
        lessonUserRepository.deleteAll();
        return 1;
    }


    @GetMapping("getAll")
    public List<CoursePacket> getAll() {
        List<Course> courses = courseRepository.findAll();
        List<CoursePacket> coursePackets = new ArrayList<>();
        for (Course course : courses) {
            CoursePacket coursePacket = courseTools.mapCourseToPacket(course);
            coursePackets.add(coursePacket);
        }
        return coursePackets;
    }

    @GetMapping("getAllCategory")
    public List<CategoryPacket> getAllCategory() {
        List<CourseCategory> courseCategories = courseCategoryRepository.findAll();
        List<CategoryPacket> categoryPackets = new ArrayList<>();
        for (CourseCategory courseCategory : courseCategories) {
            CategoryPacket categoryPacket = courseTools.mapCategoryToPacket(courseCategory);
            categoryPackets.add(categoryPacket);
        }
        return categoryPackets;
    }

    public void curseSave(Course course) {
        if (course.getLessonCourses() != null) {
            for (LessonCourse lessonCourse : course.getLessonCourses()) {
                lessonCurseSave(lessonCourse);
            }
        }
        if (course.getCategory() != null) {
            courseCategoryRepository.save(course.getCategory());
        }

        if (course.getAutor() != null) {
            userRepository.save(course.getAutor());
        }

        courseRepository.save(course);
    }


    public void lessonCurseSave(LessonCourse lessonCourse) {
        if (lessonCourse.getElementLessons() != null) {
            for (ElementLesson elementLesson : lessonCourse.getElementLessons()) {
                elementLessonSave(elementLesson);
            }
        }
        if (lessonCourse.getLessonUser() != null) {
            for (LessonUser lessonUser : lessonCourse.getLessonUser()) {
                lessonUser.setLessonCurse(lessonCourse);
                lessonUserRepository.save(lessonUser);
            }
        }
        lessonCourseRepository.save(lessonCourse);
    }

    public void elementLessonSave(ElementLesson elementLesson) {
        if (elementLesson.getQuestions() != null) {
            for (Question question : elementLesson.getQuestions()) {
                questionSave(question);
            }
        }
        elementLessonRepository.save(elementLesson);
    }

    public void questionSave(Question question) {
        if (question.getAnswers() != null) {
            for (Answer answer : question.getAnswers()) {
                answerRepository.save(answer);
            }
        }
        questionRepository.save(question);
    }

    public void questionDelete(Question question) {
        if (question.getAnswers() != null) {
            for (Answer answer : question.getAnswers()) {
                answerRepository.delete(answer);
            }
        }
        questionRepository.delete(question);
    }

    public void elementLessonDelete(ElementLesson elementLesson) {
        if (elementLesson.getQuestions() != null) {
            for (Question question : elementLesson.getQuestions()) {
                questionDelete(question);
            }
        }
        elementLessonRepository.delete(elementLesson);
    }

    public void lessonCurseDelete(LessonCourse lessonCourse) {
        if (lessonCourse.getElementLessons() != null) {
            for (ElementLesson elementLesson : lessonCourse.getElementLessons()) {
                elementLessonDelete(elementLesson);
            }
        }
        if (lessonCourse.getLessonUser() != null) {
            for (LessonUser lessonUser : lessonCourse.getLessonUser()) {
                lessonUserRepository.delete(lessonUser);
            }
        }
        lessonCourseRepository.delete(lessonCourse);
    }

    public void curseDelete(Course course) {
        if (course.getLessonCourses()!=null) {
            for (LessonCourse lessonCourse : course.getLessonCourses()) {
                lessonCurseDelete(lessonCourse);
            }
        }
        if(course.getCategory()!=null) {
            courseCategoryRepository.delete(course.getCategory());
        }
        courseRepository.delete(course);
    }


    @PostMapping("saveUserToCourse")
    public void saveUserToCourse(@RequestBody SaveUserCoursesPacket saveUserCoursesPacket) {
        Course course = courseRepository.getOne(saveUserCoursesPacket.getIdCourse());

        for (LessonCourse lessonCourse : course.getLessonCourses()) {

            LessonUser lessonUser = new LessonUser();
            lessonUser.setResult(0);
            lessonUser.setCompleted(0);

            User user = userRepository.findById(saveUserCoursesPacket.getIdUser()).get();
            lessonUser.setUser(user);


            if (lessonCourse.getLocked() == 0) {
                lessonUser.setUnlocked(1);
            } else {
                lessonUser.setUnlocked(0);
            }

            lessonUser.setLessonCurse(lessonCourse);

            lessonUserRepository.save(lessonUser);
            lessonCourseRepository.save(lessonCourse);
        }

    }

    @PostMapping("modifyLessonUser")
    public int modifyLesson(@RequestBody ModifyLessonUserPacket modifyLessonUserPacket) {
        LessonCourse lessonCourse = lessonCourseRepository.findById(modifyLessonUserPacket.getIdLesson()).get();
        User user = userRepository.findById(modifyLessonUserPacket.getIdUser()).get();
        LessonUser lessonUser = lessonUserRepository.findByUserAndLessonCurse(user, lessonCourse);

        if (modifyLessonUserPacket.getUnlock() != -1)
            lessonUser.setUnlocked(modifyLessonUserPacket.getUnlock());

        if (modifyLessonUserPacket.getResult() != -1)
            lessonUser.setResult(modifyLessonUserPacket.getResult());

        if (modifyLessonUserPacket.getCorrectQuestion() != -1)
            lessonUser.setCorrectQuestion(modifyLessonUserPacket.getCorrectQuestion());

        if (modifyLessonUserPacket.getErrorQuestion() != -1)
            lessonUser.setErrorQuestion(modifyLessonUserPacket.getErrorQuestion());

        if (modifyLessonUserPacket.getTrials() != -1)
            lessonUser.setTrials(modifyLessonUserPacket.getTrials());

        if (modifyLessonUserPacket.getCompleted() != -1)
            lessonUser.setCompleted(modifyLessonUserPacket.getCompleted());


        return 1;
    }

    public int createCategory(CategoryPacket categoryPacket) {
        CourseCategory courseCategory = courseTools.mapPacketToCategory(categoryPacket);
        courseCategoryRepository.save(courseCategory);
        return 1;
    }
}